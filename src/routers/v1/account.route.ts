import { NextFunction, Request, Response, Router } from "express";
import ResponseHandler from "@/middlewares/response.middleware";
import { v4 } from "uuid";
import { PersonCtrl } from "@/controllers/persons.controller";
import Validator from "@/middlewares/validator.middleware";
import {
  AccountExistValidation,
  AccountUpdateValidation,
  PersonRegisterValidation,
  PersonUpdateValidation,
  RecoveryPasswordAcceptValidation,
  RecoveryPasswordAttemptValidation,
  StatusValidation
} from "@/models/joi/person.joi";
import { authMiddleware, roleMiddleware } from "@/middlewares/middlewares";
import { RESPONSE_STATUS, ROLES } from "@/models/types/constants";
import { Settings } from "@/configs/settings";
import multer from "multer";
import { ERRORS } from "@/models/types/errors";
import { FileCtrl } from "@/controllers/file.controller";

const router = Router();

// configure multer
const upload = multer({
  dest: `${Settings.shared.FILE_PATH}/`,
  limits: {
    files: 1, // allow up to 1 files per request,
    fieldSize: 2 * 1024 * 1024 // 2 MB (max file size)
  },
  fileFilter: (req: any, file: any, cb: any) => {
    // allow images only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
      return cb(new Error("Only image are allowed."), false);
    }
    cb(null, true);
  }
});

/**
 * @api {post} /v1/account/register/attempt
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/register/attempt",
  Validator.joi(PersonRegisterValidation),
  (req: Request, res: Response, next: NextFunction) => {
    req.body.passwordResetToken = v4();
    req.body.passwordResetExpires = Date.now();
    PersonCtrl.register(req.body)
      .then((result) => {
        res.locals["response"] = { id: result.id };
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/account/pwd/recover/attempt
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/pwd/recover/attempt",
  Validator.joi(RecoveryPasswordAttemptValidation),
  (req: Request, res: Response, next: NextFunction) => {
    req.body.passwordResetToken = v4();
    req.body.passwordResetExpires = Date.now();
    PersonCtrl.pwdRecoveryAttempt(req.body)
      .then((result) => {
        res.locals["response"] = { id: result.id };
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/account/exists
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.get(
  "/exists",
  Validator.joi(AccountExistValidation, "query"),
  (req: Request, res: Response, next: NextFunction) => {
    const email: string = req.query["email"].toString();
    PersonCtrl.exist(email)
      .then((result) => {
        res.locals["response"] = { exist: result };
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/account/pwd/recover/accept
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/pwd/recover/accept",
  authMiddleware,
  Validator.joi(RecoveryPasswordAcceptValidation),
  (req: Request, res: Response, next: NextFunction) => {
    req.body.email = req.user["email"];
    PersonCtrl.pwdRecoveryAccept(req.body)
      .then((result) => {
        res.locals["response"] = { id: result.id };
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/account/:id/pwd/override
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/:id/pwd/override",
  authMiddleware,
  roleMiddleware(ROLES.ADMIN),
  Validator.joi(RecoveryPasswordAcceptValidation),
  (req: Request, res: Response, next: NextFunction) => {
    req.body.id = req.params["id"].toString();
    PersonCtrl.updateProfile(req.body)
      .then((result) => {
        res.locals["response"] = { id: result.id };
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {put} /v1/account/profile
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.put(
  "/profile",
  authMiddleware,
  Validator.joi(PersonUpdateValidation),
  (req: Request, res: Response, next: NextFunction) => {
    req.body.id = req.user["_id"];
    PersonCtrl.updateProfile(req.body)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {get} /v1/account/me
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.get(
  "/me",
  authMiddleware,
  (req: Request, res: Response, next: NextFunction) => {
    const id = req.user["_id"];
    PersonCtrl.getAccountById(id)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {get} /v1/account/image
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.get(
  "/image/:id",
  (req: Request, res: Response, next: NextFunction) => {
    const id = req.params["id"];
    FileCtrl.downloadImages(id)
      .then((result) => {
        res.download(result.path, result.originalname, (err) => {
          if (err && !res.headersSent) {
            next({
              error: RESPONSE_STATUS.HTTP_INTERNAL_SERVER_ERROR,
              status: RESPONSE_STATUS.HTTP_INTERNAL_SERVER_ERROR
            });
          }
        });
      })
      .catch(next);
  },
  /*ResponseHandler.success,*/
  ResponseHandler.error
);

/**
 * @api {put} /v1/account/:id
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.put(
  "/:id",
  authMiddleware,
  roleMiddleware(ROLES.ADMIN),
  Validator.joi(AccountUpdateValidation),
  (req: Request, res: Response, next: NextFunction) => {
    req.body.id = req.params["id"].toString();
    PersonCtrl.updateProfile(req.body)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/account/image
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/image",
  upload.single("image"),
  authMiddleware,
  (req: Request, res: Response, next: NextFunction) => {
    if (!req.file) {
      return next({
        error: ERRORS.FIELDS_REQUIRED,
        status: RESPONSE_STATUS.HTTP_BAD_REQUEST
      });
    }
    req.body.id = req.user["_id"].toString();
    FileCtrl.register(req.file)
      .then((f) => {
        req.body.image = f.id;
        PersonCtrl.updateProfileImage(req.body)
          .then((result) => {
            res.locals["response"] = result;
            next();
          })
          .catch(next);
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {get} /v1/account/:id
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.get(
  "/:id",
  authMiddleware,
  roleMiddleware(ROLES.ADMIN),
  (req: Request, res: Response, next: NextFunction) => {
    const id: string = req.params["id"].toString();
    PersonCtrl.getAccountById(id)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {get} /v1/account/:id/status/change
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.put(
  "/:id/status/change",
  authMiddleware,
  roleMiddleware(ROLES.ADMIN),
  Validator.joi(StatusValidation),
  (req: Request, res: Response, next: NextFunction) => {
    const data = {
      id: req.params["id"].toString(),
      status: req.body.status
    };
    PersonCtrl.updateProfile(data)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {get} /v1/account/
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.get(
  "/",
  authMiddleware,
  roleMiddleware(ROLES.ADMIN),
  (req: Request, res: Response, next: NextFunction) => {
    PersonCtrl.getAccounts()
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/account/
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/",
  Validator.joi(PersonRegisterValidation),
  authMiddleware,
  roleMiddleware(ROLES.ADMIN),
  (req: Request, res: Response, next: NextFunction) => {
    req.body.token = v4();
    req.body.status = 1;
    PersonCtrl.create(req.body)
      .then((result) => {
        res.locals["response"] = { id: result.id };
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

export default router;
