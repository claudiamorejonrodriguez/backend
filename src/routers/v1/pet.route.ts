import { Router, Request, Response, NextFunction } from "express";
import ResponseHandler from "@/middlewares/response.middleware";
import Validator from "@/middlewares/validator.middleware";
import {
  PetAcceptedDeniedInterestedValidation,
  PetRegisterValidation,
  PetUpdateValidation
} from "@/models/joi/pet.joi";
import { Settings } from "@/configs/settings";
import { PetsCtrl } from "@/controllers/pets.controller";
import { PET_CURRENT_STATE, RESPONSE_STATUS, ROLES } from "@/models/types/constants";
import { ERRORS } from "@/models/types/errors";
import { FileCtrl } from "@/controllers/file.controller";
import { authMiddleware, roleMiddleware } from "@/middlewares/middlewares";

const router = Router();
const multer = require("multer");

// configure multer
const upload = multer({
  dest: `${Settings.shared.FILE_PATH}/`,
  limits: {
    files: 1, // allow up to 1 files per request,
    fieldSize: 2 * 1024 * 1024 // 2 MB (max file size)
  },
  fileFilter: (req: any, file: any, cb: any) => {
    // allow images only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
      return cb(new Error("Only image are allowed."), false);
    }
    cb(null, true);
  }
});

const uploadMany = multer({
  dest: `${Settings.shared.FILE_PATH}/`,
  limits: {
    files: 10, // allow up to 1 files per request,
    fieldSize: 2 * 1024 * 1024 // 2 MB (max file size)
  },
  fileFilter: (req: any, file: any, cb: any) => {
    // allow images only
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
      return cb(new Error("Only image are allowed."), false);
    }
    cb(null, true);
  }
});

/**
 * @api {post} /v1/pet/register
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/register",
  upload.single("image"),
  authMiddleware,
  Validator.joi(PetRegisterValidation),
  (req: Request, res: Response, next: NextFunction) => {
    if (!req.file) {
      return next({
        error: ERRORS.FIELDS_REQUIRED,
        status: RESPONSE_STATUS.HTTP_BAD_REQUEST
      });
    }
    FileCtrl.register(req.file)
      .then((f) => {
        req.body.owner = req.user["_id"];
        req.body.image = f.id;
        PetsCtrl.register(req.body)
          .then((result) => {
            res.locals["response"] = result;
            next();
          })
          .catch(next);
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {put} /v1/pet/update/:id
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.put(
  "/update/:id",
  upload.single("image"),
  authMiddleware,
  Validator.joi(PetUpdateValidation),
  (req: Request, res: Response, next: NextFunction) => {
    req.body.id = req.params["id"];
    req.body.owner = req.user["_id"];
    req.body.status = PET_CURRENT_STATE.PENDING;
    if (req.file) {
      FileCtrl.register(req.file)
        .then((f) => {
          req.body.image = f.id;
          PetsCtrl.update(req.body)
            .then((result) => {
              res.locals["response"] = result;
              next();
            })
            .catch(next);
        })
        .catch(next);
    } else {
      PetsCtrl.update(req.body)
        .then((result) => {
          res.locals["response"] = result;
          next();
        })
        .catch(next);
    }
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/pet/:id/active
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/:id/active",
  authMiddleware,
  roleMiddleware(ROLES.ADMIN),
  (req: Request, res: Response, next: NextFunction) => {
    req.body.id = req.params["id"];
    req.body.status = PET_CURRENT_STATE.ACTIVE;
    PetsCtrl.updateStatus(req.body)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/pet/:id/block
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/:id/block",
  authMiddleware,
  roleMiddleware(ROLES.ADMIN),
  (req: Request, res: Response, next: NextFunction) => {
    req.body.id = req.params["id"];
    req.body.status = PET_CURRENT_STATE.BLOCKED;
    PetsCtrl.updateStatus(req.body)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/pet/:id/accepted
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/:id/accepted",
  authMiddleware,
  Validator.joi(PetAcceptedDeniedInterestedValidation),
  (req: Request, res: Response, next: NextFunction) => {
    req.body.id = req.params["id"];
    req.body.owner = req.user["_id"];
    PetsCtrl.accepted(req.body)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/pet/:id/denied
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/:id/denied",
  Validator.joi(PetAcceptedDeniedInterestedValidation),
  authMiddleware,
  (req: Request, res: Response, next: NextFunction) => {
    req.body.id = req.params["id"];
    req.body.owner = req.user["_id"];
    PetsCtrl.denied(req.body)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/pet/:id/interested
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/:id/interested",
  authMiddleware,
  (req: Request, res: Response, next: NextFunction) => {
    req.body.id = req.params["id"];
    req.body.person = req.user["_id"];
    PetsCtrl.interested(req.body)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {del} /v1/pet/remove/:id
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.delete(
  "/remove/:id",
  authMiddleware,
  (req: Request, res: Response, next: NextFunction) => {
    req.body.id = req.params["id"];
    req.body.owner = req.user["_id"];
    PetsCtrl.remove(req.body)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {get} /v1/pet/list
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.get(
  "/list",
  (req: Request, res: Response, next: NextFunction) => {
    PetsCtrl.getAll()
      .then((result) => {
        res.locals["response"] = { pets: result };
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {get} /v1/pet/list/admin
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.get(
  "/list/admin",
  authMiddleware,
  roleMiddleware(ROLES.ADMIN),
  (req: Request, res: Response, next: NextFunction) => {
    const query: any = {};
    if (req.query["group"] && Number.parseInt(req.query["group"].toString()) === 3) {
      try {
        query.status = { $nin: [PET_CURRENT_STATE.SOFT_DELETE] };
        if (req.query["status"]) {
          switch (Number.parseInt(req.query["status"].toString())) {
            case 0:
              query.status.$nin = [
                PET_CURRENT_STATE.SOFT_DELETE,
                PET_CURRENT_STATE.BLOCKED,
                PET_CURRENT_STATE.ENDED,
                PET_CURRENT_STATE.ACTIVE
              ];
              break;
            case 1:
              query.status.$nin = [
                PET_CURRENT_STATE.SOFT_DELETE,
                PET_CURRENT_STATE.PENDING,
                PET_CURRENT_STATE.ENDED,
                PET_CURRENT_STATE.ACTIVE
              ];
              break;
          }
        }
      } catch (e) {
        console.log(e);
      }
    }
    PetsCtrl.getAllAdmin(query)
      .then((result) => {
        res.locals["response"] = { pets: result };
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {get} /v1/pet/list/me
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.get(
  "/list/me",
  authMiddleware,
  (req: Request, res: Response, next: NextFunction) => {
    req.body.owner = req.user["_id"];
    const query: any = {};
    if (req.query["group"] && Number.parseInt(req.query["group"].toString()) === 0) {
      try {
        query["owner"] = { $ne: req.user["_id"] };
        query.interested = { $ne: req.user["_id"] };
        query.denied = { $ne: req.user["_id"] };
        query.accepted = { $ne: req.user["_id"] };
        query.status = {
          $nin: [
            PET_CURRENT_STATE.SOFT_DELETE,
            PET_CURRENT_STATE.BLOCKED,
            PET_CURRENT_STATE.ENDED,
            PET_CURRENT_STATE.PENDING
          ]
        };
      } catch (e) {
        console.log(e);
      }
    }

    if (req.query["group"] && Number.parseInt(req.query["group"].toString()) === 1) {
      try {
        query["owner"] = req.user["_id"];
        query.interested = { $ne: req.user["_id"] };
        query.denied = { $ne: req.user["_id"] };
        query.accepted = { $ne: req.user["_id"] };
        query.status = { $nin: [PET_CURRENT_STATE.SOFT_DELETE] };
        if (req.query["status"]) {
          switch (Number.parseInt(req.query["status"].toString())) {
            case 1:
              query.status.$nin = [
                PET_CURRENT_STATE.SOFT_DELETE,
                PET_CURRENT_STATE.BLOCKED,
                PET_CURRENT_STATE.ENDED,
                PET_CURRENT_STATE.PENDING
              ];
              break;
            case 2:
              query.status.$nin = [
                PET_CURRENT_STATE.SOFT_DELETE,
                PET_CURRENT_STATE.BLOCKED,
                PET_CURRENT_STATE.ENDED,
                PET_CURRENT_STATE.ACTIVE
              ];
              break;
            case 3:
              query.status.$nin = [
                PET_CURRENT_STATE.SOFT_DELETE,
                PET_CURRENT_STATE.PENDING,
                PET_CURRENT_STATE.ENDED,
                PET_CURRENT_STATE.ACTIVE
              ];
              break;
            case 4:
              query.status.$nin = [
                PET_CURRENT_STATE.SOFT_DELETE,
                PET_CURRENT_STATE.PENDING,
                PET_CURRENT_STATE.BLOCKED,
                PET_CURRENT_STATE.ACTIVE
              ];
              break;
          }
        }
      } catch (e) {
        console.log(e);
      }
    }

    if (req.query["group"] && Number.parseInt(req.query["group"].toString()) === 2) {
      try {
        query["owner"] = { $ne: req.user["_id"] };
        query.status = { $nin: [PET_CURRENT_STATE.SOFT_DELETE, PET_CURRENT_STATE.BLOCKED] };
        if (req.query["status"]) {
          switch (Number.parseInt(req.query["status"].toString())) {
            case 0:
              query.interested = req.user["_id"];
              query.denied = { $ne: req.user["_id"] };
              query.accepted = { $ne: req.user["_id"] };
              query.status.$nin = [
                PET_CURRENT_STATE.SOFT_DELETE,
                PET_CURRENT_STATE.BLOCKED,
                PET_CURRENT_STATE.ENDED,
                PET_CURRENT_STATE.PENDING
              ];
              break;
            case 1:
              query.interested = { $ne: req.user["_id"] };
              query.denied = req.user["_id"];
              query.accepted = { $ne: req.user["_id"] };
              query.status.$nin = [
                PET_CURRENT_STATE.SOFT_DELETE,
                PET_CURRENT_STATE.BLOCKED,
                PET_CURRENT_STATE.ENDED,
                PET_CURRENT_STATE.PENDING
              ];
              break;
            case 2:
              query.accepted = req.user["_id"];
              query.status.$nin = [
                PET_CURRENT_STATE.SOFT_DELETE,
                PET_CURRENT_STATE.PENDING,
                PET_CURRENT_STATE.BLOCKED,
                PET_CURRENT_STATE.ACTIVE
              ];
              break;
          }
        }
      } catch (e) {
        console.log(e);
      }
    }

    PetsCtrl.getMe(query)
      .then((result) => {
        res.locals["response"] = { pets: result };
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {get} /v1/pet/:id
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.get(
  "/:id",
  (req: Request, res: Response, next: NextFunction) => {
    req.body.id = req.params["id"];
    PetsCtrl.getById(req.body)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

export default router;
