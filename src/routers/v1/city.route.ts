import { Router, Request, Response, NextFunction } from "express";
import ResponseHandler from "@/middlewares/response.middleware";
import { CityCtrl } from "@/controllers/city.controller";

const router = Router();

router.get(
  "/:id",
  (req: Request, res: Response, next: NextFunction) => {
    CityCtrl.getById({ id: req.params["id"] })
      .then((value) => {
        res.locals["response"] = value;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

router.get(
  "/",
  (req: Request, res: Response, next: NextFunction) => {
    CityCtrl.getAll()
      .then((value) => {
        res.locals["response"] = value;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

export default router;
