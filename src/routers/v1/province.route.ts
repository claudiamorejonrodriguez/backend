import { Router, Request, Response, NextFunction } from "express";
import ResponseHandler from "@/middlewares/response.middleware";
import { ProvinceCtrl } from "@/controllers/province.controller";

const router = Router();

router.get(
  "/:id",
  (req: Request, res: Response, next: NextFunction) => {
    ProvinceCtrl.getById({ id: req.params["id"] })
      .then((value) => {
        res.locals["response"] = value;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

router.get(
  "/",
  (req: Request, res: Response, next: NextFunction) => {
    ProvinceCtrl.getAll()
      .then((value) => {
        res.locals["response"] = value;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

export default router;
