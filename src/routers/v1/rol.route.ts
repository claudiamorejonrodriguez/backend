import { NextFunction, Request, Response, Router } from "express";
import ResponseHandler from "@/middlewares/response.middleware";
import { authMiddleware, roleMiddleware } from "@/middlewares/middlewares";
import { RESPONSE_STATUS, ROLES } from "@/models/types/constants";
import { PersonCtrl } from "@/controllers/persons.controller";
import Validator from "@/middlewares/validator.middleware";
import { RolesValidation } from "@/models/joi/person.joi";

const router = Router();

/**
 * @api {put} /v1/rol/:id
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.put(
  "/:id",
  authMiddleware,
  roleMiddleware(ROLES.ADMIN),
  Validator.joi(RolesValidation),
  (req: Request, res: Response, next: NextFunction) => {
    if (!req.params["id"]) {
      next({
        error: RESPONSE_STATUS.HTTP_BAD_REQUEST,
        status: RESPONSE_STATUS.HTTP_BAD_REQUEST
      });
    }
    req.body.id = req.params["id"];
    PersonCtrl.updateRoles(req.body)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {delete} /v1/rol/:id
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.delete(
  "/:id",
  authMiddleware,
  roleMiddleware(ROLES.ADMIN),
  (req: Request, res: Response, next: NextFunction) => {
    if (!req.params["id"]) {
      next({
        error: RESPONSE_STATUS.HTTP_BAD_REQUEST,
        status: RESPONSE_STATUS.HTTP_BAD_REQUEST
      });
    }
    req.body.id = req.params["id"];
    PersonCtrl.removeRoles(req.body)
      .then((result) => {
        res.locals["response"] = result;
        next();
      })
      .catch(next);
  },
  ResponseHandler.success,
  ResponseHandler.error
);
export default router;
