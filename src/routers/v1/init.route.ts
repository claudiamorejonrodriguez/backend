import { NextFunction, Request, Response, Router } from "express";
import ResponseHandler from "@/middlewares/response.middleware";
import async from "async";
import { CityModel } from "@/models/schemas/city.model";
import { ProvinceModel } from "@/models/schemas/province.model";

const data = [
  {
    province: "Azuay",
    cities: [
      "Camilo Ponce Enríquez",
      "Chordeleg",
      "Cuenca",
      "El Pan",
      "Girón",
      "Camilo Ponce Enríquez",
      "Guachapala",
      "Gualaceo",
      "Nabón",
      "Oña",
      "Paute",
      "Pucará",
      "San Fernando",
      "Santa Isabel",
      "Sevilla De Oro",
      "Sigsig"
    ]
  },
  {
    province: "Bolívar",
    cities: ["Caluma", "Chillanes", "Chimbo", "Echeandía", "Guaranda", "Las Naves", "San Miguel"]
  },
  {
    province: "Cañar",
    cities: ["Azogues", "Biblián", "Cañar", "Déleg", "El Tambo", "La Troncal", "Suscal"]
  },
  {
    province: "Carchi",
    cities: ["Bolívar", "Espejo", "Mira", "Montúfar", "San Pedro De Huaca", "Tulcán"]
  },
  {
    province: "Chimborazo",
    cities: [
      "Alausí",
      "Chambo",
      "Chunchi",
      "Colta",
      "Cumandá",
      "Guamote",
      "Guano",
      "Pallatanga",
      "Penipe",
      "Riobamba"
    ]
  },
  {
    province: "Cotopaxi",
    cities: ["La Maná", "Latacunga", "Pangua", "Pujilí", "Salcedo", "Saquisilí", "Sigchos"]
  },
  {
    province: "El Oro",
    cities: [
      "Arenillas",
      "Atahualpa",
      "Balsas",
      "Chilla",
      "El Guabo",
      "Huaquillas",
      "Las Lajas",
      "Machala",
      "Marcabelí",
      "Pasaje",
      "Piñas",
      "Portovelo",
      "Santa Rosa",
      "Zaruma"
    ]
  },
  {
    province: "Esmeraldas",
    cities: [
      "Atacames",
      "Eloy Alfaro",
      "Esmeraldas",
      "La Concordia",
      "Muisne",
      "Quinindé",
      "Muisne",
      "Quinindé",
      "Rioverde",
      "San Lorenzo"
    ]
  },
  {
    province: "Galápagos",
    cities: ["Isabela", "San Cristóbal", "Santa Cruz"]
  },
  {
    province: "Guayas",
    cities: [
      "Alfredo Baquerizo Moreno (Juján)",
      "Balao",
      "Balzar",
      "Colimes",
      "Coronel Marcelino Maridueña",
      "Daule",
      "Durán",
      "El Empalme",
      "El Triunfo",
      "General Antonio Elizalde (Bucay)",
      "Guayaquil",
      "Isidro Ayora",
      "Lomas De Sargentillo",
      "Milagro",
      "Naranjal",
      "Naranjito",
      "Nobol",
      "Palestina",
      "Pedro Carbo",
      "Playas",
      "Salitre (Urbina Jado)",
      "Samborondón",
      "San Jacinto De Yaguachi",
      "Santa Lucía",
      "Simón Bolívar"
    ]
  },
  {
    province: "Imbabura",
    cities: ["Antonio Ante", "Cotacachi", "Ibarra", "Otavalo", "Pimampiro", "San Miguel De Urcuquí"]
  },
  {
    province: "Loja",
    cities: [
      "Calvas",
      "Catamayo",
      "Celica",
      "Chaguarpamba",
      "Espíndola",
      "Gonzanamá",
      "Loja",
      "Macará",
      "Olmedo",
      "Paltas",
      "Pindal",
      "Puyango",
      "Quilanga",
      "Saraguro",
      "Sozoranga",
      "Zapotillo"
    ]
  },
  {
    province: "Los Ríos",
    cities: [
      "Baba",
      "Babahoyo",
      "Buena Fé",
      "Mocache",
      "Montalvo",
      "Palenque",
      "Puebloviejo",
      "Quevedo",
      "Quinsaloma",
      "Urdaneta",
      "Valencia",
      "Ventanas",
      "Vinces"
    ]
  },
  {
    province: "Manabí",
    cities: [
      "24 De Mayo",
      "Bolívar",
      "Chone",
      "El Carmen",
      "Flavio Alfaro",
      "Jama",
      "Jaramijó",
      "Jipijapa",
      "Junín",
      "Manta",
      "Montecristi",
      "Olmedo",
      "Paján",
      "Pedernales",
      "Pichincha",
      "Portoviejo",
      "Puerto López",
      "Rocafuerte",
      "San Vicente",
      "Santa Ana",
      "Sucre",
      "Tosagua"
    ]
  },
  {
    province: "Morona Santiago",
    cities: [
      "Gualaquiza",
      "Huamboya",
      "Limón Indanza",
      "Logroño",
      "Morona",
      "Pablo Sexto",
      "Palora",
      "San Juan Bosco",
      "Santiago",
      "Sucúa",
      "Taisha",
      "Tiwintza"
    ]
  },
  {
    province: "Napo",
    cities: ["Archidona", "Carlos Julio Arosemena Tola", "El Chaco", "Quijos", "Tena"]
  },
  {
    province: "Orellana",
    cities: ["Aguarico", "La Joya De Los Sachas", "Loreto", "Orellana"]
  },
  {
    province: "Pastaza",
    cities: ["Arajuno", "Mera", "Pastaza", "Santa Clara"]
  },
  {
    province: "Pichincha",
    cities: [
      "Cayambe",
      "Mejía",
      "Pedro Moncayo",
      "Pedro Vicente Maldonado",
      "Puerto Quito",
      "Quito",
      "Rumiñahui",
      "San Miguel De Los Bancos"
    ]
  },
  {
    province: "Santa Elena",
    cities: ["La Libertad", "Salinas", "Santa Elena"]
  },
  {
    province: "Santo Domingo de los Tsáchilas",
    cities: ["La Concordia", "Santo Domingo"]
  },
  {
    province: "Sucumbíos",
    cities: [
      "Cascales",
      "Cuyabeno",
      "Gonzalo Pizarro",
      "Lago Agrio",
      "Putumayo",
      "Shushufindi",
      "Sucumbíos"
    ]
  },
  {
    province: "Tungurahua",
    cities: [
      "Ambato",
      "Baños De Agua Santa",
      "Cevallos",
      "Mocha",
      "Patate",
      "Quero",
      "San Pedro De Pelileo",
      "Santiago De Píllaro",
      "Tisaleo"
    ]
  },
  {
    province: "Zamora Chinchipe",
    cities: [
      "Centinela Del Cóndor",
      "Chinchipe",
      "El Pangui",
      "Nangaritza",
      "Palanda",
      "Paquisha",
      "Yacuambí",
      "Yantzaza",
      "Zamora"
    ]
  }
];

const router = Router();

router.post(
  "/",
  (req: Request, res: Response, next: NextFunction) => {
    async.forEach(
      data,
      (item, callback) => {
        const cityIDs: string[] = [];
        async.forEach(
          item.cities,
          (item1, callback1) => {
            CityModel.create({ name: item1 })
              .then((city) => {
                cityIDs.push(city.id);
                callback1();
              })
              .catch(callback1);
          },
          (err) => {
            if (err) {
              console.log(err);
              return callback(err);
            }
            ProvinceModel.create({ name: item.province }).then((province) => {
              ProvinceModel.updateOne(
                { _id: province._id },
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                { $addToSet: { cities: { $each: cityIDs } } }
              ).then(() => {
                callback();
              });
            });
          }
        );
      },
      (err) => {
        if (err) {
          console.log(err);
          return next(err);
        }
        res.locals["response"] = "ok";
        next();
      }
    );
  },
  ResponseHandler.success,
  ResponseHandler.error
);

export default router;
