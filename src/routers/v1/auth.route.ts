import { Router, Request, Response, NextFunction } from "express";
import ResponseHandler from "@/middlewares/response.middleware";
import Validator from "@/middlewares/validator.middleware";
import { AuthValidation } from "@/models/joi/person.joi";
import { authMiddleware } from "@/middlewares/middlewares";

const router = Router();

/**
 * @api {post} /v1/auth/method/confirm
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/method/confirm",
  authMiddleware,
  (req: Request, res: Response, next: NextFunction) => {
    if (req["user"]) {
      res.locals["response"] = req["user"];
    }
    next();
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {post} /v1/auth/token
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.post(
  "/token",
  authMiddleware,
  Validator.joi(AuthValidation),
  (req: Request, res: Response, next: NextFunction) => {
    res.locals["response"] = req["user"] ? req["user"] : false;
    next();
  },
  ResponseHandler.success,
  ResponseHandler.error
);

/**
 * @api {get} /v1/auth/validate
 * @apiName Client
 * @apiGroup Client
 * @apiPermission 'user'
 */
router.get(
  "/validate",
  authMiddleware,
  (req: Request, res: Response, next: NextFunction) => {
    res.locals["response"] = { valid: true };
    next();
  },
  ResponseHandler.success,
  ResponseHandler.error
);

export default router;
