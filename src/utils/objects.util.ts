class Objects {
  static get(obj, path, value) {
    if (!obj) {
      return value;
    }
    if (!path) {
      return obj;
    }
    const keys = path.split(".");
    let itr = 0;
    let tmp = obj;
    while (itr < keys.length) {
      tmp = tmp[keys[itr]];
      if (tmp === null || tmp === undefined) {
        return value;
      }
      itr++;
    }
    return tmp;
  }
  static set(obj, path, value) {
    if (!obj || !path) {
      return;
    }
    const keys = path.split(".");
    let itr = 0;
    let tmp = obj;
    while (itr < keys.length - 1) {
      if (tmp[keys[itr]] === null || tmp[keys[itr]] === undefined) {
        tmp[keys[itr]] = {};
      }
      tmp = tmp[keys[itr]];
      itr++;
    }
    tmp[keys[itr]] = value;
  }
}
export default Objects;
