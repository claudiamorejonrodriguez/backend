import { Response as ExpressResponse } from "express";
import { RESPONSE_STATUS } from "@/models/types/constants";
import { ERRORS } from "@/models/types/errors";

export class Response {
  // private static _instance: Response;
  // eslint-disable-next-line no-empty-function,@typescript-eslint/no-empty-function
  constructor() {}

  /* public static get shared(): Response {
    if (!Response._instance) {
      Response._instance = new Response();
    }
    return Response._instance;
  }*/

  /**
   * Send response with given status and data
   *
   * @param res
   * @param status
   * @param data
   */
  send(res: ExpressResponse, status: number, data: any = {}): void {
    res["response"] = data;
    res.status(status).json(res["response"]);
  }

  /**
   * Send response with status 200 and the given data
   *
   * @param res
   * @param data
   */
  ok(res: ExpressResponse, data: any = {}): void {
    this.send(res, RESPONSE_STATUS.HTTP_OK, data);
  }

  /**
   * Send response with status 200, the ID of given object and data
   *
   * @param res
   * @param obj
   * @param data
   */
  okCreate(res: ExpressResponse, obj: any, data: any = {}): void {
    data.id = obj.id;
    this.send(res, RESPONSE_STATUS.HTTP_OK, data);
  }

  /**
   * Send response with an error code and a message
   *
   * @param res
   * @param status
   * @param code
   * @param data
   */
  error(res: ExpressResponse, status: number, code: number, data: any = {}): void {
    if (code) {
      data.error = code;
    }
    this.send(res, status, data);
  }

  /**
   * Send a bad request response with given code and data
   *
   * @param res
   * @param code
   * @param data
   */
  badRequest(res: ExpressResponse, code: number, data: any = {}): void {
    this.error(res, RESPONSE_STATUS.HTTP_BAD_REQUEST, code, data);
  }

  /**
   * Send response error code 500
   *
   * @param res
   */
  error500(res: ExpressResponse): void {
    this.error(res, RESPONSE_STATUS.HTTP_INTERNAL_SERVER_ERROR, ERRORS.INTERNAL_SERVER_ERROR);
  }
}

export const ResponseAPI = new Response();
