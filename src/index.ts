import "module-alias/register";
import * as dotenv from "dotenv";
import fs from "fs";
import { Settings } from "@/configs/settings";
import cluster from "cluster";
import { App } from "@/App";
import { Server } from "@/Server";
import { mongoose } from "@typegoose/typegoose";
import Debugger from "debug";

const debug = Debugger(Settings.shared.API_NAME + ":Index");

if (fs.existsSync(".env")) {
  dotenv.config({ path: ".env" });
  debug("Using .env file to supply config environment variables");
}

Settings.shared;

/* Workers store */
const workers: {
  on: (arg0: string, arg1: (worker: any, code: any, signal: any) => void) => void;
}[] = [];

/**
 * Helper function for spawning worker at index 'i'
 */
function createWorker(i: number) {
  /* Create the worker process */
  workers[i] = cluster.fork();

  // Restart worker on exit
  workers[i].on("exit", function (worker: any, code: any, signal: any) {
    // eslint-disable-next-line no-console
    console.log(`Cluster worker ${worker.process.pid} died with code ${code} and signal ${signal}`);
    createWorker(i);
  });
}

if (cluster.isMaster) {
  /* Handle master worker */
  // eslint-disable-next-line no-console
  console.log(`Master process is running with PID ${process.pid} \n`);
  /* Initialize the workers */
  for (let i = 0; i < Settings.shared.INSTANCES; i++) {
    createWorker(i);
  }
} else if (cluster.isWorker) {
  /* Initial express app */
  const app: App = App.shared;

  /* Initial express server */
  const server: Server = Server.shared;

  /* Initialize the express app */
  app.setupExpress();

  /* Initialize MongoDB database */
  app.setupMongo(mongoose);

  /* TODO Configure express routers */
  app.setupRoutes();

  /* Start the express server */
  /* TODO Move more Server Configs to SERVER */
  server.initServer();
}
