import { ProvinceDocument, ProvinceModel } from "@/models/schemas/province.model";
import { RESPONSE_STATUS } from "@/models/types/constants";

class ProvinceController {
  private static _instance: ProvinceController;

  /**
   * Private ProvinceController constructor
   * This class will have only one instance
   */
  // eslint-disable-next-line no-empty-function,@typescript-eslint/no-empty-function
  private constructor() {}

  /**
   * Get singleton class instance
   */
  public static get shared(): ProvinceController {
    if (!ProvinceController._instance) {
      ProvinceController._instance = new ProvinceController();
    }
    return ProvinceController._instance;
  }

  public getAll(): Promise<ProvinceDocument[]> {
    return new Promise<ProvinceDocument[]>((resolve, reject) => {
      ProvinceModel.find({})
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_OK
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }

  public getById(data: any): Promise<ProvinceDocument> {
    return new Promise<ProvinceDocument>((resolve, reject) => {
      ProvinceModel.findOne({ _id: data.id })
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_OK
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }
}

export const ProvinceCtrl: ProvinceController = ProvinceController.shared;
