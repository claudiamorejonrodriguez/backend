import { RESPONSE_STATUS } from "@/models/types/constants";
import { CityDocument, CityModel } from "@/models/schemas/city.model";

class CityController {
  private static _instance: CityController;

  /**
   * Private CityController constructor
   * This class will have only one instance
   */
  // eslint-disable-next-line no-empty-function,@typescript-eslint/no-empty-function
  private constructor() {}

  /**
   * Get singleton class instance
   */
  public static get shared(): CityController {
    if (!CityController._instance) {
      CityController._instance = new CityController();
    }
    return CityController._instance;
  }

  public getAll(): Promise<CityDocument[]> {
    return new Promise<CityDocument[]>((resolve, reject) => {
      CityModel.find({})
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_OK
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }

  public getById(data: any): Promise<CityDocument> {
    return new Promise<CityDocument>((resolve, reject) => {
      CityModel.findOne({ _id: data.id })
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_OK
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }
}

export const CityCtrl: CityController = CityController.shared;
