import { RESPONSE_STATUS } from "@/models/types/constants";
import { ERRORS } from "@/models/types/errors";

class Errors {
  private static _instance: Errors;

  /**
   * Private UserController constructor
   * This class will have only one instance
   */
  // eslint-disable-next-line no-empty-function,@typescript-eslint/no-empty-function
  private constructor() {}

  /**
   * Get singleton class instance
   */
  public static get shared(): Errors {
    if (!Errors._instance) {
      Errors._instance = new Errors();
    }
    return Errors._instance;
  }

  public parseError(err) {
    const error = {
      status: err.status ? err.status : RESPONSE_STATUS.HTTP_BAD_REQUEST,
      response: {
        error: err.error ? err.error : ERRORS.INTERNAL_SERVER_ERROR
      }
    };
    if (err.data) {
      error.response["data"] = err.data;
    }
    if (err.name === "MongoError") {
      switch (err.code) {
        case 11000:
          error.response.error = ERRORS.OBJECT_DUPLICATED;
          error.status = RESPONSE_STATUS.HTTP_CONFLICT;
          break;
        default:
          error.response.error = ERRORS.INVALID_OPERATION;
          error.status = RESPONSE_STATUS.HTTP_BAD_REQUEST;
      }
    } else {
      if (err.code) {
        switch (err.code) {
          case 401:
            error.response.error = ERRORS.INVALID_OPERATION;
            error.status = RESPONSE_STATUS.HTTP_UNAUTHORIZED;
            break;
          default:
            error.response.error = ERRORS.INVALID_OPERATION;
            error.status = err.status || RESPONSE_STATUS.HTTP_FORBIDDEN;
        }
      }
    }
    return error;
  }
}

export const ErrorCtrl = Errors.shared;
