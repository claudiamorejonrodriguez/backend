import { PET_CURRENT_STATE, RESPONSE_STATUS } from "@/models/types/constants";
import { Pets, PetsDocument, PetsModel } from "@/models/schemas/pets.model";
import { createTransport } from "nodemailer";
import { PersonCtrl } from "@/controllers/persons.controller";

class PetsController {
  private static _instance: PetsController;

  /**
   * Private PetController constructor
   * This class will have only one instance
   */
  // eslint-disable-next-line no-empty-function,@typescript-eslint/no-empty-function
  private constructor() {}

  /**
   * Get singleton class instance
   */
  public static get shared(): PetsController {
    if (!PetsController._instance) {
      PetsController._instance = new PetsController();
    }
    return PetsController._instance;
  }

  /**
   * Create new Pet.
   */
  public register(data: Pets): Promise<PetsDocument> {
    return new Promise<PetsDocument>((resolve, reject) => {
      if (data) {
        PetsModel.create(data)
          .then((result) => {
            resolve(result);
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * Accept request Pet.
   */
  public accepted(data: any): Promise<PetsDocument> {
    return new Promise<PetsDocument>((resolve, reject) => {
      if (data) {
        PersonCtrl.getAccountById(data.person)
          .then((person) => {
            if (!person) {
              return reject({
                error: RESPONSE_STATUS.HTTP_BAD_REQUEST,
                status: RESPONSE_STATUS.HTTP_NOT_FOUND
              });
            }
            PetsModel.findOneAndUpdate(
              { _id: data.id, owner: data.owner, status: PET_CURRENT_STATE.ACTIVE },
              { accepted: data.person, status: PET_CURRENT_STATE.ENDED },
              { new: true }
            )
              .then((result) => {
                if (!result) {
                  return reject({
                    error: RESPONSE_STATUS.HTTP_NOT_FOUND,
                    status: RESPONSE_STATUS.HTTP_OK
                  });
                }
                const transporter = createTransport({
                  service: "gmail",
                  auth: {
                    user: process.env.GMAIL_USER,
                    pass: process.env.GMAIL_PASSWORD
                  }
                });
                const mailOptions = {
                  to: person.email,
                  from: "pest@starter.com",
                  subject: "Your are accepted",
                  text: `You are receiving this email because you are accepted:\n\n                        
                         Pet: ${result.id}.\n\n              
                        If you did not request this, please ignore this email.\n`
                };
                transporter.sendMail(mailOptions, (err) => {
                  resolve(result);
                });
              })
              .catch((reason) => {
                reject({
                  error: RESPONSE_STATUS.HTTP_CONFLICT,
                  status: RESPONSE_STATUS.HTTP_BAD_REQUEST
                });
              });
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * interested request Pet.
   */
  public interested(data: any): Promise<PetsDocument> {
    return new Promise<PetsDocument>((resolve, reject) => {
      if (data) {
        PersonCtrl.getAccountById(data.person)
          .then((person) => {
            if (!person) {
              return reject({
                error: RESPONSE_STATUS.HTTP_BAD_REQUEST,
                status: RESPONSE_STATUS.HTTP_NOT_FOUND
              });
            }
            PetsModel.findOneAndUpdate(
              { _id: data.id, status: PET_CURRENT_STATE.ACTIVE },
              { $addToSet: { interested: data.person } },
              { new: true }
            )
              .then((result) => {
                if (!result) {
                  return reject({
                    error: RESPONSE_STATUS.HTTP_NOT_FOUND,
                    status: RESPONSE_STATUS.HTTP_OK
                  });
                }
                resolve(result);
              })
              .catch((reason) => {
                reject({
                  error: RESPONSE_STATUS.HTTP_CONFLICT,
                  status: RESPONSE_STATUS.HTTP_BAD_REQUEST
                });
              });
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * denied request Pet.
   */
  public denied(data: any): Promise<PetsDocument> {
    return new Promise<PetsDocument>((resolve, reject) => {
      if (data) {
        PersonCtrl.getAccountById(data.person)
          .then((person) => {
            if (!person) {
              return reject({
                error: RESPONSE_STATUS.HTTP_BAD_REQUEST,
                status: RESPONSE_STATUS.HTTP_NOT_FOUND
              });
            }
            PetsModel.findOneAndUpdate(
              { _id: data.id, owner: data.owner, status: PET_CURRENT_STATE.ACTIVE },
              { $pull: { interested: data.person } },
              { new: true }
            )
              .then(() => {
                PetsModel.findOneAndUpdate(
                  { _id: data.id, owner: data.owner, status: PET_CURRENT_STATE.ACTIVE },
                  { $addToSet: { denied: data.person } },
                  { new: true }
                )
                  .then((result) => {
                    if (!result) {
                      return reject({
                        error: RESPONSE_STATUS.HTTP_NOT_FOUND,
                        status: RESPONSE_STATUS.HTTP_OK
                      });
                    }
                    const transporter = createTransport({
                      service: "gmail",
                      auth: {
                        user: process.env.GMAIL_USER,
                        pass: process.env.GMAIL_PASSWORD
                      }
                    });
                    const mailOptions = {
                      to: person.email,
                      from: "pest@starter.com",
                      subject: "Your are denied",
                      text: `You are receiving this email because you are denied:\n\n                        
                         Pet: ${result.id}.\n\n              
                        If you did not request this, please ignore this email.\n`
                    };
                    transporter.sendMail(mailOptions, (err) => {
                      resolve(result);
                    });
                  })
                  .catch((reason) => {
                    reject({
                      error: RESPONSE_STATUS.HTTP_CONFLICT,
                      status: RESPONSE_STATUS.HTTP_BAD_REQUEST
                    });
                  });
              })
              .catch((reason) => {
                reject({
                  error: RESPONSE_STATUS.HTTP_CONFLICT,
                  status: RESPONSE_STATUS.HTTP_BAD_REQUEST
                });
              });
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * Update Pet.
   */
  public update(data: any): Promise<PetsDocument> {
    return new Promise<PetsDocument>((resolve, reject) => {
      if (data) {
        PetsModel.findOneAndUpdate(
          { _id: data.id, owner: data.owner, status: PET_CURRENT_STATE.ACTIVE },
          data,
          { new: true }
        )
          .then((result) => {
            if (!result) {
              return reject({
                error: RESPONSE_STATUS.HTTP_NOT_FOUND,
                status: RESPONSE_STATUS.HTTP_OK
              });
            }
            resolve(result);
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * Update status Pet.
   */
  public updateStatus(data: any): Promise<PetsDocument> {
    return new Promise<PetsDocument>((resolve, reject) => {
      if (data) {
        PetsModel.findOneAndUpdate(
          {
            _id: data.id,
            status: { $nin: [PET_CURRENT_STATE.ENDED, PET_CURRENT_STATE.SOFT_DELETE] }
          },
          { status: data.status },
          { new: true }
        )
          .then((result) => {
            if (!result) {
              return reject({
                error: RESPONSE_STATUS.HTTP_NOT_FOUND,
                status: RESPONSE_STATUS.HTTP_OK
              });
            }
            resolve(result);
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * Remove Pet.
   */
  public remove(data: any): Promise<PetsDocument> {
    return new Promise<PetsDocument>((resolve, reject) => {
      if (data) {
        PetsModel.findOneAndUpdate(
          {
            _id: data.id,
            owner: data.owner
          },
          { status: PET_CURRENT_STATE.SOFT_DELETE },
          { new: true }
        )
          .then((result) => {
            if (!result) {
              return reject({
                error: RESPONSE_STATUS.HTTP_NOT_FOUND,
                status: RESPONSE_STATUS.HTTP_OK
              });
            }
            resolve(result);
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * get all Pet.
   */
  public getAll(): Promise<PetsDocument[]> {
    return new Promise<PetsDocument[]>((resolve, reject) => {
      PetsModel.find({ status: PET_CURRENT_STATE.ACTIVE })
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_OK
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }

  /**
   * get all me Pet.
   */
  public getMe(data: any): Promise<PetsDocument[]> {
    return new Promise<PetsDocument[]>((resolve, reject) => {
      PetsModel.find(data)
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_OK
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }

  /**
   * get all admin Pet.
   */
  public getAllAdmin(data: any): Promise<PetsDocument[]> {
    return new Promise<PetsDocument[]>((resolve, reject) => {
      PetsModel.find(data)
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_OK
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }

  /**
   * get by id Pet.
   */
  public getById(data: any): Promise<PetsDocument> {
    return new Promise<PetsDocument>((resolve, reject) => {
      PetsModel.findOne({ _id: data.id, status: { $ne: PET_CURRENT_STATE.SOFT_DELETE } })
        .populate("interested")
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_OK
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }
}

export const PetsCtrl: PetsController = PetsController.shared;
