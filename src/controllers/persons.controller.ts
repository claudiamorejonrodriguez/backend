import { Person, PersonDocument, PersonModel } from "@/models/schemas/person.model";
import { createTransport } from "nodemailer";
import { v4 } from "uuid";
import { PERSON_CURRENT_STATE, RESPONSE_STATUS } from "@/models/types/constants";

class PersonController {
  private static _instance: PersonController;

  /**
   * Private UserController constructor
   * This class will have only one instance
   */
  // eslint-disable-next-line no-empty-function,@typescript-eslint/no-empty-function
  private constructor() {}

  /**
   * Get singleton class instance
   */
  public static get shared(): PersonController {
    if (!PersonController._instance) {
      PersonController._instance = new PersonController();
    }
    return PersonController._instance;
  }

  /**
   * Register new Person.
   */
  public register(data: Person): Promise<PersonDocument> {
    return new Promise<PersonDocument>((resolve, reject) => {
      if (data) {
        PersonModel.create(data)
          .then((result) => {
            const transporter = createTransport({
              service: "gmail",
              auth: {
                user: process.env.GMAIL_USER,
                pass: process.env.GMAIL_PASSWORD
              }
            });
            const mailOptions = {
              to: result.email,
              from: "pest@starter.com",
              subject: "Complete register process",
              text: `Hello,\n\nThis is a confirmation code for your account ${result.passwordResetToken}.\n`
            };
            transporter.sendMail(mailOptions, (err) => {
              resolve(result);
            });
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * Create new Person.
   */
  public create(data: Person): Promise<PersonDocument> {
    return new Promise<PersonDocument>((resolve, reject) => {
      if (data) {
        PersonModel.create(data)
          .then((result) => {
            resolve(result);
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * pwd recover attempt.
   */
  public pwdRecoveryAttempt(data: Person): Promise<PersonDocument> {
    return new Promise<PersonDocument>((resolve, reject) => {
      if (data) {
        PersonModel.findOneAndUpdate(
          { email: data.email },
          {
            passwordResetToken: data.passwordResetToken,
            passwordResetExpires: data.passwordResetExpires,
            token: v4()
          },
          { new: true }
        )
          .then((result) => {
            const transporter = createTransport({
              service: "gmail",
              auth: {
                user: process.env.GMAIL_USER,
                pass: process.env.GMAIL_PASSWORD
              }
            });
            const mailOptions = {
              to: result.email,
              from: "pest@starter.com",
              subject: "Reset your password",
              text: `You are receiving this email because you (or someone else) have requested the reset of the password for your account.\n\n          
              Please copy the following code, to complete the process:\n\n
                        
              ${result.passwordResetToken}.\n\n
              
              If you did not request this, please ignore this email and your password will remain unchanged.\n`
            };
            transporter.sendMail(mailOptions, (err) => {
              resolve(result);
            });
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * pwd recover accept.
   */
  public pwdRecoveryAccept(data: Person): Promise<PersonDocument> {
    return new Promise<PersonDocument>((resolve, reject) => {
      if (data) {
        PersonModel.findOneAndUpdate(
          { email: data.email },
          {
            password: data.password
          },
          { new: true }
        )
          .then((result) => {
            resolve(result);
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * Exist Person by email.
   */
  public exist(data: string): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      if (data) {
        PersonModel.findOne({ email: data })
          .then((result) => {
            resolve(!!result);
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        resolve(false);
      }
    });
  }

  /**
   * profile update.
   */
  public updateProfile(data: any): Promise<PersonDocument> {
    return new Promise<PersonDocument>((resolve, reject) => {
      if (data) {
        PersonModel.findOneAndUpdate({ _id: data.id }, data, { new: true })
          .then((result) => {
            if (!result) {
              return reject({
                error: RESPONSE_STATUS.HTTP_NOT_FOUND,
                status: RESPONSE_STATUS.HTTP_OK
              });
            }
            resolve(result);
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * image profile update.
   */
  public updateProfileImage(data: any): Promise<PersonDocument> {
    return new Promise<PersonDocument>((resolve, reject) => {
      if (data) {
        PersonModel.findOneAndUpdate({ _id: data.id }, data, { new: true })
          .then((result) => {
            if (!result) {
              return reject({
                error: RESPONSE_STATUS.HTTP_NOT_FOUND,
                status: RESPONSE_STATUS.HTTP_OK
              });
            }
            resolve(result);
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * get account by id.
   */
  public getAccountById(data: string): Promise<PersonDocument> {
    return new Promise<PersonDocument>((resolve, reject) => {
      PersonModel.findOne({ _id: data })
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_OK
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }

  /**
   * profile update.
   */
  public getAccounts(): Promise<PersonDocument[]> {
    return new Promise<PersonDocument[]>((resolve, reject) => {
      PersonModel.find()
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_OK
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }

  /**
   * roles update.
   */
  public updateRoles(data: any): Promise<PersonDocument> {
    return new Promise<PersonDocument>((resolve, reject) => {
      PersonModel.findOneAndUpdate(
        { _id: data.id, status: PERSON_CURRENT_STATE.ACTIVE },
        { $addToSet: { roles: { $each: data.roles } } },
        { new: true }
      )
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_NOT_FOUND
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }

  /**
   * roles remove.
   */
  public removeRoles(data: any): Promise<PersonDocument> {
    return new Promise<PersonDocument>((resolve, reject) => {
      PersonModel.findOneAndUpdate(
        { _id: data.id, status: PERSON_CURRENT_STATE.ACTIVE },
        { $pullAll: { roles: data.roles } },
        { new: true }
      )
        .then((result) => {
          if (!result) {
            return reject({
              error: RESPONSE_STATUS.HTTP_NOT_FOUND,
              status: RESPONSE_STATUS.HTTP_OK
            });
          }
          resolve(result);
        })
        .catch((reason) => {
          reject({
            error: RESPONSE_STATUS.HTTP_CONFLICT,
            status: RESPONSE_STATUS.HTTP_BAD_REQUEST
          });
        });
    });
  }
}

export const PersonCtrl: PersonController = PersonController.shared;
