import { v4 } from "uuid";
import { FileDocument, FileModel } from "@/models/schemas/file.model";
import * as fs from "fs";
import * as async from "async";
import { RESPONSE_STATUS } from "@/models/types/constants";

class FileController {
  private static _instance: FileController;

  // eslint-disable-next-line no-empty-function,@typescript-eslint/no-empty-function
  private constructor() {}

  /**
   * Get singleton class instance
   */
  public static get shared(): FileController {
    if (!FileController._instance) {
      FileController._instance = new FileController();
    }
    return FileController._instance;
  }

  public generateRandomCode = (cb: any) => {
    /* Generate the new short code */
    const code = v4();
    /* Check if the short code exists */
    FileModel.findOne({ key: code }, { _id: true }).then((file) => {
      if (!file) {
        return cb(code);
      }

      return this.generateRandomCode(cb);
    });
  };

  /**
   * POST /file
   * Register new file.
   */
  public register(data: any): Promise<FileDocument> {
    return new Promise<FileDocument>((resolve, reject) => {
      /* Register the new item */
      if (data) {
        this.generateRandomCode((code: any) => {
          const logoObj: any = {
            fieldname: data.filename,
            originalname: data.originalname,
            encoding: data.encoding,
            mimetype: data.mimetype,
            destination: data.destination,
            filename: data.filename,
            path: data.path,
            size: data.size,
            key: code
          };
          FileModel.create(logoObj).then(resolve).catch(reject);
        });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  /**
   * POST /files
   * Register new files.
   */
  public registerMany(data: any): Promise<any[]> {
    return new Promise<any[]>((resolve, reject) => {
      const res_ids: any[] = [];
      async.forEach(
        data,
        (image: any, cback: any) => {
          FileCtrl.generateRandomCode((code: any) => {
            const imageObj: File | any = {
              fieldname: image.filename,
              originalname: image.originalname,
              encoding: image.encoding,
              mimetype: image.mimetype,
              destination: image.destination,
              filename: image.filename,
              path: image.path,
              size: image.size,
              key: code
            };
            FileModel.create(imageObj as any)
              .then((result) => {
                res_ids.push(result._id);
              })
              .catch(cback);
          });
        },
        (err) => {
          if (err) {
            return reject(err);
          }
          resolve(res_ids);
        }
      );
    });
  }

  /**
   * DELETE /file
   * Delete exist file.
   */
  public delete(id: string): Promise<FileDocument> {
    return new Promise<FileDocument>((resolve, reject) => {
      if (id) {
        FileModel.findByIdAndDelete(id as any)
          .then((r) => {
            if (fs.existsSync(r.path)) {
              fs.rmSync(r.path);
              resolve(r);
            } else {
              resolve(r);
            }
          })
          .catch((reason) => {
            reject({
              error: RESPONSE_STATUS.HTTP_CONFLICT,
              status: RESPONSE_STATUS.HTTP_BAD_REQUEST
            });
          });
      } else {
        reject({
          error: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
          status: RESPONSE_STATUS.HTTP_OK
        });
      }
    });
  }

  public downloadImages(data: string): Promise<FileDocument> {
    return new Promise<FileDocument>((resolve, reject) => {
      FileModel.findOne({ _id: data }).then((result) => {
        if (!result) {
          reject({
            error: RESPONSE_STATUS.HTTP_NOT_FOUND,
            status: RESPONSE_STATUS.HTTP_OK
          });
        }
        resolve(result);
      });
    });
  }
}

export const FileCtrl = FileController.shared;
