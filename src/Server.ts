import { App } from "./App";
import { Settings } from "@/configs/settings";

import Debugger from "debug";
const debug = Debugger("Pets:Server");

export class Server {
  app: App = App.shared;
  private static _instance: Server;

  public static get shared(): Server {
    if (!Server._instance) {
      Server._instance = new Server();
    }
    return Server._instance;
  }

  /**
   * Start Express server.
   */

  public initServer(): void {
    this.app.app.listen(this.app.app.get("port"), () => {
      debug(
        `App is running at http://127.0.0.1:${this.app.app.get("port")} in ${this.app.app.get(
          "env"
        )} mode`
      );
      debug(`Version ${Settings.shared.API_VERSION}`);
      debug("Press CTRL-C to stop");
    });
  }
}
