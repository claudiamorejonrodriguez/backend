import express from "express";
import logger from "morgan";
import cors from "cors";

import Debugger from "debug";
import Helmet from "helmet";
import NoCache from "nocache";
import onFinished from "on-finished";
import * as moment from "moment";
import { Settings } from "@/configs/settings";
import passport from "passport";
import PersonRouter from "./routers/v1/account.route";
import AuthRouter from "./routers/v1/auth.route";
import PetRouter from "./routers/v1/pet.route";
import RolRouter from "./routers/v1/rol.route";
import InitRouter from "./routers/v1/init.route";
import ProvinceRouter from "./routers/v1/province.route";
import CityRouter from "./routers/v1/city.route";

const compression = require("compression");
const debug = Debugger("Pets:App");

export class App {
  public app: express.Application;

  private static _instance: App;

  public static get shared(): App {
    if (!App._instance) {
      App._instance = new App();
    }
    return App._instance;
  }

  public setupExpress(): void {
    this.app = express();
    this.app.use(logger("dev"));

    const options: any = { limit: "512MB", extended: true };
    if (process.env.HTTP_BODY_SIZE) {
      options["limit"] = process.env.HTTP_BODY_SIZE;
    }

    /* Enable JSON parser */
    this.app.use(express.json(options));

    /* Enable URL encoded parser */
    options["extended"] = true;
    this.app.use(express.urlencoded(options));

    this.app.use(cors());
    this.app.use(compression());

    require("./middlewares/middlewares");
    this.app.use(passport.initialize());

    /**
     * Security mechanism
     */
    this.app.use(Helmet.xssFilter()); // Prevent XSS attack
    this.app.disable("x-powered-by"); // Disable header X-Powered-By: Express
    this.app.use(Helmet.frameguard({ action: "deny" })); // Prevent clickjacking attack
    this.app.use(NoCache());
    this.app.use(Helmet.hidePoweredBy()); // Update the powered by
    this.app.use(Helmet.noSniff()); // Setting header X-Content-Type-Options: nosniff

    /* Express basic configuration */
    this.app.set("port", Settings.shared.PORT);
    this.app.set("env", process.env.ENV || "dev");

    /* Increment debug output on devel offline platforms */
    if (process.env.ENV === "dev-offline" || process.env.ENV === "dev") {
      this.app.all("/*", function (req, res, next) {
        onFinished(res, function (err, resp) {
          if (err) {
            debug(err);
            return;
          }

          const requestTrace = {
            stamp: moment.utc().toDate().getTime(),
            req: {
              method: req.method,
              url: req.originalUrl,
              body: req.body,
              headers: req.headers
            },
            res: {
              status: resp.statusCode,
              message: resp.statusMessage,
              body: resp["response"] ? resp["response"] : null
            }
          };
          debug(" === Request (" + requestTrace.stamp + "): " + JSON.stringify(requestTrace));
        });
        next();
      });
    }
  }

  public setupMongo(mongoose: {
    set: (arg0: string, arg1: boolean) => void;
    connect: (arg0: any, arg1: { useNewUrlParser: boolean; useUnifiedTopology: boolean }) => void;
    connection: { on: (arg0: string, arg1: any) => void };
  }) {
    /* Mongoose initialization */
    mongoose.set("useCreateIndex", true);
    mongoose.set("useFindAndModify", false);

    mongoose.connect(Settings.shared.MONGODB_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
    // eslint-disable-next-line no-console
    mongoose.connection.on("error", console.error.bind(console, "MongoDB connection error:"));
  }

  public setupRoutes() {
    /* Routes initialization */
    this.app.use("/v1/account", PersonRouter);
    this.app.use("/v1/auth/", AuthRouter);
    this.app.use("/v1/pet/", PetRouter);
    this.app.use("/v1/rol/", RolRouter);
    this.app.use("/v1/init", InitRouter);
    this.app.use("/v1/province", ProvinceRouter);
    this.app.use("/v1/city", CityRouter);
  }
}
