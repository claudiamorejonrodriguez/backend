import { RESPONSE_STATUS } from "@/models/types/constants";
import { ERRORS } from "@/models/types/errors";
import Joi from "joi";

class Validator {
  static _handleErr(err) {
    const fields = [];
    (err.details || []).forEach((value) => {
      if (Array.isArray(value["path"]) && value["path"].length > 0) {
        fields.push(value["path"][value["path"].length - 1]);
      }
    });
    return {
      status: RESPONSE_STATUS.HTTP_NOT_ACCEPTABLE,
      error: ERRORS.FIELDS_REQUIRED,
      data: { fields: fields }
    };
  }
  static joi(schema, reqField = "body") {
    return (req, _res, next) => {
      const reqTmp = req;
      try {
        Joi.attempt(reqTmp[reqField], schema, { abortEarly: false, convert: true });
      } catch (err) {
        next(Validator._handleErr(err));
        return;
      }
      next();
    };
  }
}
export default Validator;
