import Debugger from "debug";
import { Request, Response, NextFunction } from "express";
import { Settings } from "@/configs/settings";
import passport from "passport";
import { RESPONSE_STATUS, ROLES } from "@/models/types/constants";
import Objects from "@/utils/objects.util";

const debug = Debugger(Settings.shared.API_NAME + ":authValidator");

/**
 * Middleware to authenticate user
 *
 * @param req
 * @param res
 * @param next
 * @returns
 */
export function authMiddleware(req: Request, res: Response, next: NextFunction) {
  /* Check for fake data, only for local testing/development */
  /*if (process.env.FAKE_UID) {
    res.locals["auth"] = {
      userId: process.env.FAKE_UID,
      roles: [process.env.FAKE_ROLE]
    };
    return next();
  }*/

  require("../configs/passport");
  let authHeader = ["none"];

  if (req.headers["authorization"]) {
    authHeader = req.headers["authorization"].split(" ");
  } else {
    return passport.authenticate("local", { session: false })(req, res, next);
  }

  if (authHeader.length !== 2) {
    return next({
      error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
      status: RESPONSE_STATUS.HTTP_FORBIDDEN
    });
  }

  switch (authHeader[0]) {
    /* case "Basic":
      return passport.authenticate("local", { session: false })(req, res, next);*/
    case "Bearer":
      return passport.authenticate("bearer", { session: false })(req, res, next);
    case "jwt":
      return passport.authenticate("jwt", { session: false })(req, res, next);
    default:
      return next({
        error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
        status: RESPONSE_STATUS.HTTP_FORBIDDEN
      });
  }
}

/**
 * Autentica un usuario que debe tener determinado rol
 * @param role rol a verificar
 * @returns
 */
export function roleMiddleware(role?: ROLES) {
  return [
    (req: Request, res: Response, next: NextFunction) => {
      /* Check if the authenticated user holds the given role */
      if (role) {
        if (Objects.get(res, "req.user.roles", []).indexOf(role) >= 0) {
          return next();
        }
        /* User don't hold the required role */
        return next({
          error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
          status: RESPONSE_STATUS.HTTP_FORBIDDEN
        });
      }

      /* If role is not given only authenticate the user */
      next();
    }
  ];
}
