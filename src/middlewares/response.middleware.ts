import { RESPONSE_STATUS } from "@/models/types/constants";
import { ErrorCtrl } from "@/controllers/error.controller";

class ResponseHandler {
  static success(_req, res, _next) {
    res.status(RESPONSE_STATUS.HTTP_OK).json(res.locals["response"]).end();
  }
  static errorParse(err, _req, res, next) {
    res.locals["error"] = ErrorCtrl.parseError(err);
    next(res.locals["error"]);
  }
  static error(err, _req, res, _next) {
    const errObj = ErrorCtrl.parseError(err);
    res.status(errObj.status).json(errObj.response).end();
  }
}
export default ResponseHandler;
