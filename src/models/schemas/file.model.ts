import { prop, getModelForClass, DocumentType, mongoose } from "@typegoose/typegoose";

export class File {
  @prop({ required: true })
  public fieldname!: string;

  @prop({ required: true })
  public originalname!: string;

  @prop({ required: true })
  public encoding!: string;

  @prop({ required: true })
  public mimetype!: string;

  @prop({ required: true })
  public destination!: string;

  @prop({ required: true })
  public filename!: string;

  @prop({ required: true })
  public path!: string;

  @prop({ required: true, unique: true })
  public key!: string;

  @prop({ required: true })
  public size!: number;

  /**
   * Get the mongoose data model
   */
  static get shared() {
    return getModelForClass(File, {
      schemaOptions: {
        collection: "files",
        timestamps: true,
        toJSON: {
          virtuals: true,
          versionKey: false,
          transform: (_doc: any, ret: any) => {
            return {
              id: ret.id,
              name: ret.name,
              field: ret.field
            };
          }
        }
      },
      options: { automaticName: false }
    });
  }
}

export type FileDocument = DocumentType<File>;
export const FileModel: mongoose.Model<FileDocument> = File.shared;
