import { prop, getModelForClass, mongoose, DocumentType, Ref, plugin } from "@typegoose/typegoose";
import {
  PET_CURRENT_STATE,
  PET_SIZE,
  PETS_ALLERGIES,
  PETS_CHRONIC,
  PETS_GENDER,
  PETS_STERILIZED,
  PETS_TREATMENT,
  PETS_VACCINATIONS
} from "@/models/types/constants";
import { File } from "@/models/schemas/file.model";
import { Person } from "@/models/schemas/person.model";
import mongoosePaginate from "mongoose-paginate-v2";
import { Province } from "@/models/schemas/province.model";
import { City } from "@/models/schemas/city.model";
import autopopulate from "mongoose-autopopulate";

@plugin(autopopulate)
@plugin(mongoosePaginate)
export class Pets {
  @prop({ required: true, index: true, ref: () => Person })
  public owner: Ref<Person>;

  @prop({ required: true })
  public age: number;

  @prop({ required: true })
  public weight: number;

  @prop({ enum: PET_SIZE, default: PET_SIZE.NOT_DEFINED })
  public size: number;

  @prop({ enum: PETS_GENDER, default: PETS_GENDER.NOT_DEFINED })
  public gender: number;

  @prop({ required: true, index: true, trim: true })
  public description: string;

  @prop({ required: true, index: true, trim: true })
  public race: string;

  @prop({ required: true, trim: true })
  public color: string;

  @prop({ enum: PETS_ALLERGIES, default: PETS_ALLERGIES.NOT_DEFINED })
  public allergies: number;

  @prop({ trim: true })
  public allergiesDescription: string;

  @prop({ enum: PETS_TREATMENT, default: PETS_TREATMENT.NOT_DEFINED })
  public receivesTreatment: number;

  @prop({ trim: true })
  public receivesTreatmentDescription: string;

  @prop({ enum: PETS_CHRONIC, default: PETS_CHRONIC.NOT_DEFINED })
  public chronicIllness: number;

  @prop({ trim: true })
  public chronicIllnessDescription: string;

  @prop({ enum: PETS_VACCINATIONS, default: PETS_VACCINATIONS.NOT_DEFINED })
  public vaccinationsReceived: number;

  @prop({ trim: true })
  public vaccinationsReceivedDescription: string;

  @prop({ enum: PETS_STERILIZED, default: PETS_STERILIZED.NOT_DEFINED })
  public sterilized: number;

  @prop({ ref: () => Province, required: true, autopopulate: true })
  public province: Ref<Province>;

  @prop({ ref: () => City, required: true, autopopulate: true })
  public city: Ref<City>;

  @prop({ default: 1 })
  public amount?: number;

  @prop({ default: 1 })
  public remain?: number;

  @prop({ ref: () => File, required: true })
  public image?: Ref<File>;

  @prop({ ref: () => File, default: [] })
  public images?: Ref<File>[];

  @prop({ enum: PET_CURRENT_STATE, default: PET_CURRENT_STATE.PENDING })
  public status?: PET_CURRENT_STATE;

  @prop({ ref: () => Person, default: [], autopopulate: true })
  public interested?: Ref<Person>[];

  @prop({ ref: () => Person, default: [], autopopulate: true })
  public denied?: Ref<Person>[];

  @prop({ ref: () => Person, autopopulate: true })
  public accepted?: Ref<Person>;

  /**
   * Get the mongoose data model
   */
  static get shared() {
    return getModelForClass(Pets, {
      schemaOptions: {
        collection: "pets",
        timestamps: true,
        toJSON: {
          virtuals: true,
          versionKey: false,
          transform: (_doc: any, ret: any) => {
            return {
              id: ret.id,
              owner: ret.owner,
              age: ret.age,
              weight: ret.weight,
              size: ret.size,
              gender: ret.gender,
              description: ret.description,
              race: ret.race,
              color: ret.color,
              allergies: ret.allergies,
              allergiesDescription:
                ret.allergies && ret.allergies === PETS_ALLERGIES.YES
                  ? ret.allergiesDescription
                  : "",
              receivesTreatment: ret.receivesTreatment,
              receivesTreatmentDescription:
                ret.receivesTreatment && ret.receivesTreatment === PETS_TREATMENT.YES
                  ? ret.receivesTreatmentDescription
                  : "",
              chronicIllness: ret.chronicIllness,
              chronicIllnessDescription:
                ret.chronicIllness && ret.chronicIllness === PETS_CHRONIC.YES
                  ? ret.chronicIllnessDescription
                  : "",
              vaccinationsReceived: ret.vaccinationsReceived,
              vaccinationsReceivedDescription:
                ret.vaccinationsReceived && ret.vaccinationsReceived === PETS_VACCINATIONS.YES
                  ? ret.vaccinationsReceived
                  : "",
              sterilized: ret.sterilized,
              province: ret.province,
              city: ret.city,
              amount: ret.amount,
              remain: ret.remain,
              images: ret.images,
              image: ret.image,
              interested: ret.interested,
              denied: ret.denied,
              accepted: ret.accepted,
              status: ret.status
            };
          }
        }
      },
      options: { automaticName: false }
    });
  }
}

export type PetsDocument = DocumentType<Pets>;
export const PetsModel: mongoose.Model<PetsDocument> = Pets.shared;
