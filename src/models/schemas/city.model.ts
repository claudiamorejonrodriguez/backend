import { prop, getModelForClass, mongoose, DocumentType, plugin } from "@typegoose/typegoose";
import mongoosePaginate from "mongoose-paginate-v2";

@plugin(mongoosePaginate)
export class City {
  @prop({ required: true, index: true, trim: true })
  public name: string;

  /**
   * Get the mongoose data model
   */
  static get shared() {
    return getModelForClass(City, {
      schemaOptions: {
        collection: "city",
        timestamps: true,
        toJSON: {
          virtuals: true,
          versionKey: false,
          transform: (_doc: any, ret: any) => {
            return {
              id: ret.id,
              name: ret.name
            };
          }
        }
      },
      options: { automaticName: false }
    });
  }
}

export type CityDocument = DocumentType<City>;
export const CityModel: mongoose.Model<CityDocument> = City.shared;
