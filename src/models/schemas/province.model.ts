import { prop, getModelForClass, mongoose, DocumentType, Ref, plugin } from "@typegoose/typegoose";
import autopopulate from "mongoose-autopopulate";
import mongoosePaginate from "mongoose-paginate-v2";
import { City } from "@/models/schemas/city.model";

@plugin(autopopulate)
@plugin(mongoosePaginate)
export class Province {
  @prop({ required: true, index: true, trim: true })
  public name: string;

  @prop({ ref: () => City, default: [], autopopulate: true })
  public cities?: Ref<City>[];

  /**
   * Get the mongoose data model
   */
  static get shared() {
    return getModelForClass(Province, {
      schemaOptions: {
        collection: "province",
        timestamps: true,
        toJSON: {
          virtuals: true,
          versionKey: false,
          transform: (_doc: any, ret: any) => {
            return {
              id: ret.id,
              name: ret.name,
              cities: ret.cities
            };
          }
        }
      },
      options: { automaticName: false }
    });
  }
}

export type ProvinceDocument = DocumentType<Province>;
export const ProvinceModel: mongoose.Model<ProvinceDocument> = Province.shared;
