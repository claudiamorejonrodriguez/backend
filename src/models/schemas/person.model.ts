import {
  prop,
  getModelForClass,
  mongoose,
  DocumentType,
  pre,
  plugin,
  Ref
} from "@typegoose/typegoose";
import bcrypt from "bcrypt-nodejs";
import mongoosePaginate from "mongoose-paginate-v2";
import autopopulate from "mongoose-autopopulate";
//import { FilterQuery, PaginateOptions, PaginateResult } from 'mongoose';
import { PERSON_CURRENT_STATE, PERSONS_GENDER, ROLES } from "@/models/types/constants";
import { File } from "@/models/schemas/file.model";
import { Province } from "@/models/schemas/province.model";
import { City } from "@/models/schemas/city.model";

/*type PaginateMethod<T> = (
  query?: FilterQuery<T>,
  options?: PaginateOptions,
  callback?: (err: any, result: PaginateResult<T>) => void
) => Promise<PaginateResult<T>>;*/

@pre<Person>("save", function save(next) {
  const person = this as PersonDocument;
  if (!person.isModified("password")) {
    return next();
  }
  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      return next(err);
    }
    bcrypt.hash(person.password, salt, undefined, (err: mongoose.Error, hash) => {
      if (err) {
        return next(err);
      }
      person.password = hash;
      next();
    });
  });
})
@pre<Person>("findOneAndUpdate", function (next) {
  const person = this as any;
  if (person._update && person._update.password) {
    bcrypt.genSalt(10, (err, salt) => {
      if (err) {
        return next(err);
      }
      bcrypt.hash(person._update.password, salt, undefined, (err: mongoose.Error, hash) => {
        if (err) {
          return next(err);
        }
        person._update.password = hash;
        next();
      });
    });
  } else {
    next();
  }
})
@plugin(autopopulate)
@plugin(mongoosePaginate)
export class Person {
  @prop({ required: true, unique: true, index: true, trim: true })
  public email!: string;

  @prop({ required: true, index: true, trim: true })
  public name: string;

  @prop({ required: true, index: true, trim: true })
  public lastnames: string;

  @prop({ required: true, index: true })
  public phone: string;

  @prop({ required: true, index: true })
  public identificationCard: string;

  @prop({ required: true, index: true })
  public dateOfBirth: Date;

  @prop({ required: true, index: true })
  public address: string;

  @prop({ ref: () => Province, required: true, index: true, autopopulate: true })
  public province: Ref<Province>;

  @prop({ ref: () => City, required: true, index: true, autopopulate: true })
  public city: Ref<City>;

  @prop({ enum: PERSONS_GENDER, default: PERSONS_GENDER.NOT_DEFINED })
  public gender?: PERSONS_GENDER;

  @prop()
  public password?: string;

  @prop()
  public passwordResetToken?: string;

  @prop()
  public passwordResetExpires?: Date;

  @prop({ trim: true })
  public token?: string;

  @prop({ enum: PERSON_CURRENT_STATE, default: PERSON_CURRENT_STATE.PENDING })
  public status?: PERSON_CURRENT_STATE;

  @prop({ enum: ROLES, default: [ROLES.USER], type: () => [Number] })
  public roles?: ROLES[];

  @prop({ ref: () => File })
  public image?: Ref<File>;

  public async comparePassword(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, (err: mongoose.Error, isMatch: boolean) => {
      cb(err, isMatch);
    });
  }

  //static paginate: PaginateMethod<Person>;

  /**
   * Get the mongoose data model
   */
  static get shared() {
    return getModelForClass(Person, {
      schemaOptions: {
        collection: "person",
        timestamps: true,
        toJSON: {
          virtuals: true,
          versionKey: false,
          transform: (_doc: any, ret: any) => {
            return {
              id: ret.id,
              email: ret.email,
              name: ret.name,
              lastnames: ret.lastnames,
              tokens: ret.tokens,
              status: ret.status,
              roles: ret.roles,
              phone: ret.phone,
              image: ret.image,
              identificationCard: ret.identificationCard,
              dateOfBirth: ret.dateOfBirth,
              address: ret.address,
              province: ret.province,
              city: ret.city
            };
          }
        }
      },
      options: { automaticName: false }
    });
  }
}

export type PersonDocument = DocumentType<Person>;
export const PersonModel: mongoose.Model<PersonDocument> = Person.shared;
