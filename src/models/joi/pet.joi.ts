import Joi from "joi";

export const PetRegisterScheme = {
  description: Joi.string().trim().required(),
  age: Joi.number().integer().required(),
  weight: Joi.number().required(),
  size: Joi.number().integer().optional(),
  gender: Joi.number().integer().optional(),
  allergies: Joi.number().integer().optional(),
  receivesTreatment: Joi.number().integer().optional(),
  chronicIllness: Joi.number().integer().optional(),
  vaccinationsReceived: Joi.number().integer().optional(),
  sterilized: Joi.number().integer().optional(),
  race: Joi.string().trim().required(),
  color: Joi.string().trim().required(),
  allergiesDescription: Joi.string().trim().optional(),
  receivesTreatmentDescription: Joi.string().trim().optional(),
  chronicIllnessDescription: Joi.string().trim().optional(),
  vaccinationsReceivedDescription: Joi.string().trim().optional(),
  province: Joi.string().trim().required(),
  city: Joi.string().trim().required(),
  image: Joi.optional(),
  id: Joi.optional()
};

export const PetUpdateScheme = {
  description: Joi.string().trim().optional(),
  age: Joi.number().integer().optional(),
  weight: Joi.number().optional(),
  size: Joi.number().integer().optional(),
  gender: Joi.number().integer().optional(),
  allergies: Joi.number().integer().optional(),
  receivesTreatment: Joi.number().integer().optional(),
  chronicIllness: Joi.number().integer().optional(),
  vaccinationsReceived: Joi.number().integer().optional(),
  sterilized: Joi.number().integer().optional(),
  race: Joi.string().trim().optional(),
  color: Joi.string().trim().optional(),
  allergiesDescription: Joi.string().trim().optional(),
  receivesTreatmentDescription: Joi.string().trim().optional(),
  chronicIllnessDescription: Joi.string().trim().optional(),
  vaccinationsReceivedDescription: Joi.string().trim().optional(),
  province: Joi.string().trim().optional(),
  city: Joi.string().trim().optional(),
  image: Joi.optional(),
  id: Joi.optional()
};

export const PetAcceptedDeniedInterestedScheme = {
  person: Joi.string().trim().required()
};

export const PetRegisterValidation = Joi.object().keys(PetRegisterScheme);
export const PetUpdateValidation = Joi.object().keys(PetUpdateScheme);
export const PetAcceptedDeniedInterestedValidation = Joi.object().keys(
  PetAcceptedDeniedInterestedScheme
);
