import Joi from "joi";
import { PERSON_CURRENT_STATE } from "@/models/types/constants";

export const personRegisterScheme = {
  name: Joi.string().trim().required(),
  lastnames: Joi.string().trim().required(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .required(),
  phone: Joi.string().trim().required(),
  password: Joi.string().trim().required(),
  identificationCard: Joi.string().trim().required(),
  address: Joi.string().trim().required(),
  province: Joi.string().trim().required(),
  city: Joi.string().trim().required(),
  dateOfBirth: Joi.date().required()
};

export const PersonRegisterValidation = Joi.object().keys(personRegisterScheme);

export const ChangePasswordValidation = Joi.object().keys({
  oldPassword: Joi.string().required(),
  newPassword: Joi.string().required()
});

export const RecoveryPasswordAcceptValidation = Joi.object().keys({
  password: Joi.string().required()
});

export const RecoveryPasswordAttemptValidation = Joi.object().keys({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .required()
});

export const AccountExistValidation = Joi.object().keys({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .required()
});

export const PersonUpdateScheme = {
  name: Joi.string().trim().optional(),
  lastnames: Joi.string().trim().optional(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .optional(),
  phone: Joi.string().trim().optional(),
  identificationCard: Joi.string().trim().optional(),
  address: Joi.string().trim().optional(),
  province: Joi.string().trim().optional(),
  city: Joi.string().trim().optional(),
  dateOfBirth: Joi.date().optional()
};

export const PersonUpdateValidation = Joi.object().keys(PersonUpdateScheme);

export const AccountUpdateScheme = {
  name: Joi.string().trim().optional(),
  lastnames: Joi.string().trim().optional(),
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .optional(),
  phone: Joi.string().trim().optional(),
  status: Joi.number().integer().optional(),
  roles: Joi.array().items(Joi.number().integer()).optional(),
  password: Joi.string().optional(),
  identificationCard: Joi.string().trim().optional(),
  address: Joi.string().trim().optional(),
  province: Joi.string().trim().optional(),
  city: Joi.string().trim().optional(),
  dateOfBirth: Joi.date().optional()
};

export const AccountUpdateValidation = Joi.object().keys(AccountUpdateScheme);

export const AuthValidation = Joi.object().keys({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .required(),
  password: Joi.string().required()
});

export const RolesValidation = Joi.object().keys({
  roles: Joi.array().items(Joi.number().integer()).required()
});

export const StatusValidation = Joi.object().keys({
  status: Joi.number()
    .integer()
    .valid(
      PERSON_CURRENT_STATE.ACTIVE,
      PERSON_CURRENT_STATE.BLOCKED,
      PERSON_CURRENT_STATE.SOFT_DELETE
    )
    .required()
});
