/**
 * Response status used in HTTP responses
 */
export enum RESPONSE_STATUS {
  HTTP_OK = 200,
  HTTP_CREATED = 201,
  HTTP_PARTIAL_CONTENT = 206,
  HTTP_BAD_REQUEST = 400,
  HTTP_UNAUTHORIZED = 401,
  HTTP_FORBIDDEN = 403,
  HTTP_NOT_FOUND = 404,
  HTTP_NOT_ACCEPTABLE = 406,
  HTTP_CONFLICT = 409,
  HTTP_INTERNAL_SERVER_ERROR = 500
}

/**
 * Person state predefined values
 */
export enum PERSON_CURRENT_STATE {
  SOFT_DELETE = -1,
  BLOCKED,
  ACTIVE,
  PENDING
}

/**
 * Pet state predefined values
 */
export enum PET_CURRENT_STATE {
  SOFT_DELETE = -1,
  BLOCKED,
  ACTIVE,
  PENDING,
  ENDED
}

/**
 * Pet size predefined values
 */
export enum PET_SIZE {
  NOT_DEFINED = 0,
  LITTLE,
  MEDIUM,
  BIG
}

/**
 * Person roles predefined values
 */
export enum ROLES {
  NOT_DEFINED = 0,
  USER = 8,
  ADMIN = 32
  //MANAGER = 256,
  //SUPER_USER = 512
}

/**
 * Person gender predefined values
 */
export enum PERSONS_GENDER {
  NOT_DEFINED = 0,
  MALE = 1,
  FEMALE = 2
}

/**
 * Pets gender predefined values
 */
export enum PETS_GENDER {
  NOT_DEFINED = 0,
  MALE = 1,
  FEMALE = 2
}

/**
 * Pets allergies predefined values
 */
export enum PETS_ALLERGIES {
  NOT_DEFINED = 0,
  YES = 1,
  NO = 2
}

/**
 * Pets treatment predefined values
 */
export enum PETS_TREATMENT {
  NOT_DEFINED = 0,
  YES = 1,
  NO = 2
}

/**
 * Pets CHRONIC predefined values
 */
export enum PETS_CHRONIC {
  NOT_DEFINED = 0,
  YES = 1,
  NO = 2
}

/**
 * Pets VACCINATIONS predefined values
 */
export enum PETS_VACCINATIONS {
  NOT_DEFINED = 0,
  YES = 1,
  NO = 2
}

/**
 * Pets sterilized predefined values
 */
export enum PETS_STERILIZED {
  NOT_DEFINED = 0,
  YES = 1,
  NO = 2
}

/**
 * Person marital status predefined values
 */
export enum PERSONS_MARITAL_STATUS {
  NOT_DEFINED = 0,
  SINGLE = 1,
  MARRIED = 2,
  DIVORCED = 3
}
