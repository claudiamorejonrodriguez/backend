import * as os from "os";

export class Settings {
  private static _instance: Settings;

  /*    Service variables   */
  FAKE_AUTH: string = process.env["FAKE_AUTH"] || "";
  SECRET = "nodeauthsecret";
  INTERFACE: string = process.env["INTERFACE"] || "127.0.0.1";
  PORT: number = Number(process.env["PORT"]) || 5069;
  NODE_ENV: string = process.env["NODE_ENV"] || "development";
  INSTANCES: number = Number(process.env["INSTANCES"]) || os.cpus().length;
  LOG: string = process.env["LOG"] || "error";
  BODY_TRACE: string = process.env["BODY_TRACE"] || "true";
  RESPONSE_TRACE: string = process.env["RESPONSE_TRACE"] || "true";

  prod: boolean = this.NODE_ENV === "production"; // Anything else is treated as 'dev'

  /*   Database environment variables   */
  MONGODB_URI: string = this.prod ? process.env["MONGO_URI"] : process.env["MONGODB_URI_LOCAL"];

  /* Version components */
  VERSION_MAIN = 1;
  VERSION_MINOR = 0;
  VERSION_REVISION = 0;

  /* API constant information */
  API_NAME = "Pets";
  API_FULLNAME = "Pets";
  API_VERSION = `${this.VERSION_MAIN}.${this.VERSION_MINOR}.${this.VERSION_REVISION}`;
  API_COPYRIGHT = "Copyright (C) 2021";

  /* File Uploads dir */
  FILE_PATH: string = process.env["FILE_PATH"] || "uploads";

  static get shared() {
    if (!Settings._instance) {
      Settings._instance = new Settings();
    }
    return Settings._instance;
  }
}
