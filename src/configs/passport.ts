import passport from "passport";
import { PersonDocument, PersonModel } from "@/models/schemas/person.model";
import { v4 } from "uuid";
import { Settings } from "@/configs/settings";
import { PERSON_CURRENT_STATE, RESPONSE_STATUS } from "@/models/types/constants";

const LocalStrategy = require("passport-local").Strategy;
const BearerStrategy = require("passport-http-bearer").Strategy;
const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJWT = require("passport-jwt").ExtractJwt;
const jwt = require("jsonwebtoken");

passport.serializeUser((user: any, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

/**
 * Sign in using Email and Password.
 */
passport.use(
  new LocalStrategy(
    { usernameField: "email", passwordField: "password" },
    (email, password, done) => {
      PersonModel.findOne({ email: email.toLowerCase(), status: PERSON_CURRENT_STATE.ACTIVE })
        .then((person: PersonDocument) => {
          if (!person) {
            return done(
              {
                error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
                status: RESPONSE_STATUS.HTTP_FORBIDDEN
              },
              false,
              { message: `Email ${email} not found.` }
            );
          }

          person.comparePassword(password, (err: Error, isMatch: boolean) => {
            if (err) {
              return done({
                error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
                status: RESPONSE_STATUS.HTTP_FORBIDDEN
              });
            }
            if (isMatch) {
              // We don't want to store the sensitive information such as the
              // user password in the token so we pick only the email and id
              const payload = {
                token: person.token
              };
              jwt.sign(
                payload,
                `${Settings.shared.SECRET}`,
                { expiresIn: "24h" },
                (errr, token) => {
                  if (errr) {
                    return done(
                      {
                        error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
                        status: RESPONSE_STATUS.HTTP_FORBIDDEN
                      },
                      false,
                      { message: "Error occurred" }
                    );
                  }

                  return done(null, {
                    person: person,
                    accessToken: `jwt ${token}`,
                    expiresIn: `24h`
                  });
                }
              );
            } else {
              return done(
                {
                  error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
                  status: RESPONSE_STATUS.HTTP_FORBIDDEN
                },
                false,
                { message: "Correo o contraseña incorrecta." }
              );
            }
          });
        })
        .catch(done);
    }
  )
);

/**
 * Sign in using bearer Token.
 */
passport.use(
  new BearerStrategy({ realm: "", passReqToCallback: true }, (req, idToken, done) => {
    PersonModel.findOne({
      passwordResetToken: idToken.toLowerCase()
      //status: PERSON_CURRENT_STATE.PENDING
    })
      .then((person) => {
        if (!person) {
          return done(
            {
              error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
              status: RESPONSE_STATUS.HTTP_FORBIDDEN
            },
            false,
            { message: `Code ${idToken} invalid.` }
          );
        }
        PersonModel.findOneAndUpdate(
          { _id: person.id },
          {
            passwordResetToken: null,
            passwordResetExpires: null,
            token: v4(),
            status: PERSON_CURRENT_STATE.ACTIVE
          },
          { new: true }
        )
          .then((r) => {
            // We don't want to store the sensitive information such as the
            // user password in the token so we pick only the email and id
            const payload = {
              token: r.token
            };
            jwt.sign(payload, `${Settings.shared.SECRET}`, { expiresIn: "24h" }, (errr, token) => {
              if (errr) {
                return done(
                  {
                    error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
                    status: RESPONSE_STATUS.HTTP_FORBIDDEN
                  },
                  false,
                  { message: "Error occurred" }
                );
              }

              return done(undefined, {
                person: r,
                accessToken: `jwt ${token}`,
                expiresIn: `24h`
              });
            });
          })
          .catch((reason) => {
            return done({
              error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
              status: RESPONSE_STATUS.HTTP_FORBIDDEN
            });
          });
      })
      .catch((reason) => {
        return done({
          error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
          status: RESPONSE_STATUS.HTTP_FORBIDDEN
        });
      });
  })
);

/**
 * Sign in using JWT Token.
 * */
// This verifies that the token sent by the user is valid
passport.use(
  new JwtStrategy(
    {
      secretOrKey: `${Settings.shared.SECRET}`,
      jwtFromRequest: ExtractJWT.fromAuthHeaderWithScheme("jwt"),
      ignoreExpiration: true
    },
    (payload, done) => {
      // Find the user associated with the email provided by the user
      PersonModel.findOne({ token: payload.token, status: PERSON_CURRENT_STATE.ACTIVE })
        .then((person) => {
          if (!person) {
            // If the user isn't found in the database, return a message
            return done(
              {
                error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
                status: RESPONSE_STATUS.HTTP_FORBIDDEN
              },
              false,
              { message: "User not found" }
            );
          }
          // Send the user information to the next middleware
          return done(null, person, { message: "Logged in Successfully" });
        })
        .catch((reason) => {
          done(
            {
              error: RESPONSE_STATUS.HTTP_UNAUTHORIZED,
              status: RESPONSE_STATUS.HTTP_FORBIDDEN
            },
            false,
            { message: "Error" }
          );
        });
    }
  )
);
