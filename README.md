Pets API
----

env variables:
-------------------------


Service variables examples

INTERFACE = 127.0.0.1

PORT = 5069

NODE_ENV = development

INSTANCES = 2

MONGODB_URI=mongodb://localhost:27017/pets

MONGODB_URI_LOCAL=mongodb://localhost:27017/pets

DEBUG=Pets*

FILE_PATH=uploads

GMAIL_USER = username@gmail.com

GMAIL_PASSWORD = user_password

For devel use:

FAKE_UID= fake_uid

FAKE_ROLE= fake_role

Register:
-------------------------

1- Recoge los datos fundamentales, creación de la cuenta y envio del correo con el código de confirmación, no requiere
auth

http://localhost:5069/v1/account/register/attempt

2- Recoge el código de confirmación y de ser correcto devuelve el token de auth JWT, requiere auth bearer con el token
otp

http://localhost:5069/v1/auth/method/confirm

Cambio de contraseña por parte del usuario:
-------------------------

1- Recoge la nueva contraseña, requiere auth JWT

http://localhost:5069/v1/account/pwd/recover/accept

Reseteo de contraseña por parte del usuario:
-------------------------

1- Recoge el correo y envia el codigo al mismo, no requiere auth

http://localhost:5069/v1/account/pwd/recover/attempt

2- Recoge el código de confirmación y de ser correcto devuelve el token de auth JWT, requiere auth bearer con el token
otp

http://localhost:5069/v1/auth/method/confirm

3- Recoge la nueva contraseña, requiere auth JWT

http://localhost:5069/v1/account/pwd/recover/accept

Buscar si existe una cuanta:
-------------------------

1- Espera el email query param, no requiere auth

http://localhost:5069/v1/account/exists?email=root@gmail.com

Modificar el profile por parte del usuario:
-------------------------

1- Recoge los datos del profile, requiere auth JWT

http://localhost:5069/v1/account/profile

Obtener profile de un usuario por id:
-------------------------

1- Retorna los datos del profile, requiere auth JWT and rol admin

http://localhost:5069/v1/account/:id

Obtener profile de un usuario:
-------------------------

1- Retorna los datos del profile, requiere auth JWT

http://localhost:5069/v1/account/me

Obtener Todos los profile de usuario:
-------------------------

1- Retorna un arreglo de profiles de usuario, requiere auth JWT

http://localhost:5069/v1/account

Login:
-------------------------

1- Recoge email y password, no requiere auth

http://localhost:5069/v1/auth/token

Actulizar Profile por parte del admin:
-------------------------

1- Recoge id del profile, requiere auth jwt and rol admin

http://localhost:5069/v1/account/:id

Crear Profile por parte del admin:
-------------------------

1- Recoge id del profile, requiere auth jwt and rol admin

http://localhost:5069/v1/account/:id

Cambiar contraseña:
-------------------------

1- Requiere auth jwt

http://localhost:5069/v1/account/pwd/recover/accept

Reescribir contraseña por parte del admin:
-------------------------

1- Requiere auth jwt y rol admin

http://localhost:5069/v1/account/:id/pwd/override

Crear Pet:
-------------------------

1- Requiere auth jwt

http://localhost:5069/v1/pet/register

Actualizar Pet:
-------------------------

1- Recoge id de la pet, requiere auth jwt

http://localhost:5069/v1/pet/update/:id

Eliminar Pet:
-------------------------

1- Recoge id de la pet, requiere auth jwt

http://localhost:5069/v1/pet/remove/:id

listar Pet:
-------------------------

1- No requiere auth jwt

http://localhost:5069/v1/pet/list

listar mis Pet:
-------------------------

1- requiere auth jwt

http://localhost:5069/v1/pet/list/me

listar Pet para admin:
-------------------------

1- requiere auth jwt, rol admin

http://localhost:5069/v1/pet/admin

listar Pet para admin:
-------------------------

1- requiere auth jwt, rol admin

http://localhost:5069/v1/pet/admin

Obtener Pet:
-------------------------

1- no requiere auth

http://localhost:5069/v1/pet/:id

Activar Pet:
-------------------------

1- requiere auth jwt, rol admin

http://localhost:5069/v1/pet/:id/active

Block Pet:
-------------------------

1- requiere auth jwt, rol admin

http://localhost:5069/v1/pet/:id/block

Aceptar Pet Request:
-------------------------

1- requiere auth jwt

http://localhost:5069/v1/pet/:id/accepted

Denied Pet Request:
-------------------------

1- requiere auth jwt

http://localhost:5069/v1/pet/:id/deneid

Interested Pet Request:
-------------------------

1- requiere auth jwt

http://localhost:5069/v1/pet/:id/interested

